/**
 * Copyright (c) 2019 Kim Chau Duong
 * All rights reserved
 */
package nl.bioinf.bioview.metadataapi;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Class to test the unique tags REST controllers and their HATEOAS pagination.
 * unique tags will have pagination in the directory/image 'properties' modal in the main application
 *
 * @author Kim Chau Duong
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MetadataApiApplication.class)
@AutoConfigureMockMvc
public class ImageAttributeUniqueTagsTest {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("build/generated-snippets");

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    /**
     * Tests the response of the unique image tags controller
     * Should return the initial page containing 20 unique tags max, and the pagination of the current page, next page and previous page
     * @throws Exception
     */
    @Test
    public void testUniqueTagsOfImageSunny() throws Exception{
        String responseJson = "{\"content\":[\"LowPh\",\"testTag1\",\"Bloodcell\",\"testTag2\",\"testTag3\",\"testTag4\",\"dupe testTag5\",\"testTag6\",\"testTag7\",\"testTag8\",\"testTag9\",\"testTag10\",\"testTag11\",\"testTag12\",\"testTag13\",\"testTag14\",\"testTag15\",\"testTag16\",\"testTag17\",\"testTag18\"]," +
                "\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=0&size=20\"}," +
                "{\"rel\":\"next\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=1&size=20\"}," +
                "{\"rel\":\"previous\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=0&size=20\"}]}";
        this.mockMvc.perform(get("/api/metadata/image/tags?id={image_id}", 3))
                .andExpect(status().isOk())
                .andExpect(content().json((responseJson)))
                .andDo(document("unique-image-tags",
                        requestParameters(
                                parameterWithName("id").description("ID of image file that contains ROIs with tags")
                        ),
                        responseFields(
                                fieldWithPath("content").description("All unique tags found in ROIs of an image"),
                                fieldWithPath("links[].rel").description("Relation of page link with current link"),
                                fieldWithPath("links[].href").description("Link to a page related to current link")
                        )));
    }

    /**
     * Tests the json response of the second page of the previous test, skipping the first 20 unique tags,
     * and the pagination of the current page, next page and previous page
     * @throws Exception
     */
    @Test
    public void testUniqueTagsOfImageNextSunny() throws Exception{
        String responseJson = "{\"content\":[\"tag that should be on the next page\",\"tag on other roi but same image as roi 2\"]," +
                "\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=1&size=20\"}," +
                "{\"rel\":\"next\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=2&size=20\"}," +
                "{\"rel\":\"previous\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=0&size=20\"}]}";
        this.mockMvc.perform(get("/api/metadata/image/tags?id={image_id}&page={page}&size={size}", 3, 1, 20))
                .andExpect(status().isOk())
                .andExpect(content().json((responseJson)))
                .andDo(document("unique-image-tags-next",
                        requestParameters(
                                parameterWithName("id").description("ID of image file that contains ROIs with tags"),
                                parameterWithName("page").description("Current page of the tag list"),
                                parameterWithName("size").description("Amount of tags shown per page")
                        ),
                        responseFields(
                                fieldWithPath("content").description("All unique tags found in ROIs of an image"),
                                fieldWithPath("links[].rel").description("Relation of page link with current link"),
                                fieldWithPath("links[].href").description("Link to a page related to current link")
                        )));
    }

    /**
     * Tests what happens when given an image id or page that doesn't exist
     * Should return a json with empty 'content'
     * and the pagination of the current page, next page and previous page
     * @throws Exception
     */
    @Test
    public void testUniqueTagsOfImageEmpty() throws Exception{
        String responseJson = "{\"content\":[]," +
                "\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=5&size=20\"}," +
                "{\"rel\":\"next\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=6&size=20\"}," +
                "{\"rel\":\"previous\",\"href\":\"http://localhost:8080/api/metadata/image/tags?id=3&page=4&size=20\"}]}";
        this.mockMvc.perform(get("/api/metadata/image/tags?id={image_id}&page={page}&size={size}", 3, 5, 20))
                .andExpect(status().isOk())
                .andExpect(content().json((responseJson)))
                .andDo(document("unique-image-tags-empty",
                        requestParameters(
                                parameterWithName("id").description("ID of image file that contains ROIs with tags"),
                                parameterWithName("page").description("Current page of the tag list"),
                                parameterWithName("size").description("Amount of tags shown per page")
                        ),
                        responseFields(
                                fieldWithPath("content").description("All unique tags found in ROIs of an image"),
                                fieldWithPath("links[].rel").description("Relation of page link with current link"),
                                fieldWithPath("links[].href").description("Link to a page related to current link")
                        )));
    }

    /**
     * Tests the response of the unique directory tags controller
     * Should return the initial page containing 20 unique tags max, and the pagination of the current page, next page and previous page
     * @throws Exception
     */
    @Test
    public void testUniqueTagsOfDirectorySunny() throws Exception{
        String responseJson = "{\"content\":[\"LowPh\",\"testTag1\",\"Bloodcell\",\"Unique tag test\"]," +
                "\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/api/metadata/directory/tags?path=root/folder1&page=0&size=20\"}," +
                "{\"rel\":\"next\",\"href\":\"http://localhost:8080/api/metadata/directory/tags?path=root/folder1&page=1&size=20\"}," +
                "{\"rel\":\"previous\",\"href\":\"http://localhost:8080/api/metadata/directory/tags?path=root/folder1&page=0&size=20\"}]}";
        this.mockMvc.perform(get("/api/metadata/directory/tags?path={path}", "root/folder1"))
                .andExpect(status().isOk())
                .andExpect(content().json((responseJson)))
                .andDo(document("unique-dir-tags",
                        requestParameters(
                                parameterWithName("path").description("Path containing images with ROIs with tags")
                        ),
                        responseFields(
                                fieldWithPath("content").description("All unique tags found in ROIs of images in a directory"),
                                fieldWithPath("links[].rel").description("Relation of page link with current link"),
                                fieldWithPath("links[].href").description("Link to a page related to current link")
                        )));
    }

    /**
     * Tests response of unique directory tags controller when a path or page is given that doesn't exist
     * Should return a json with empty 'content'
     * and the pagination of the current page, next page and previous page
     * @throws Exception
     */
    @Test
    public void testUniqueTagsOfDirectoryEmpty() throws Exception{
        String responseJson = "{\"content\":[]," +
                "\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost:8080/api/metadata/directory/tags?path=root/folder1/fake-folder&page=0&size=20\"}," +
                "{\"rel\":\"next\",\"href\":\"http://localhost:8080/api/metadata/directory/tags?path=root/folder1/fake-folder&page=1&size=20\"}," +
                "{\"rel\":\"previous\",\"href\":\"http://localhost:8080/api/metadata/directory/tags?path=root/folder1/fake-folder&page=0&size=20\"}]}";
        this.mockMvc.perform(get("/api/metadata/directory/tags?path={path}", "root/folder1/fake-folder"))
                .andExpect(status().isOk())
                .andExpect(content().json((responseJson)))
                .andDo(document("unique-dir-tags-empty",
                        requestParameters(
                                parameterWithName("path").description("Path containing images with ROIs with tags")
                        ),
                        responseFields(
                                fieldWithPath("content").description("All unique tags found in ROIs of images in a directory"),
                                fieldWithPath("links[].rel").description("Relation of page link with current link"),
                                fieldWithPath("links[].href").description("Link to a page related to current link")
                        )));
    }

    /**
     * Checks the unique directory tags output when a path is given that contains a backslash
     * Should return a BAD_REQUEST response
     * @throws Exception
     */
    @Test
    public void testMyResourcePathBackSlash() throws Exception{
        this.mockMvc.perform(get("/api/metadata/directory/tags?path={path}", "root\\folder1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest());
    }
}
