package nl.bioinf.bioview.metadataapi.service;

import nl.bioinf.bioview.metadataapi.Model.*;

import nl.bioinf.bioview.metadataapi.data_access.jdbc.ImageDataDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class ImageMetaDataService implements ImageDataService{
    private final ImageDataDataSource imageDataDataSource;

    @Autowired
    public ImageMetaDataService(ImageDataDataSource imageDataDataSource){
        this.imageDataDataSource = imageDataDataSource;
    }


    @Override
    public List<CompleteRoi> searchRegionOfInterest(HashMap<String, Range> searchRanges, List<String> tags, int page, int size) {
        return imageDataDataSource.searchRegionOfInterest(searchRanges, tags, page, size);
    }

    public ImageTags getTaggedImage(int id) {
        return imageDataDataSource.getTaggedImage(id);
    }

    @Override
    public List<String> getAvailableTags() {
        return imageDataDataSource.getAvailableTags();
    }

    @Override
    public void addNewTag(int image_id, String tag, String uploader) {
        imageDataDataSource.insertNewTag(image_id, tag, uploader);
    }

    @Override
    public void addNewState(regionOfInterestState regionOfInterestState, int roiID) {
        imageDataDataSource.addNewRoiState(regionOfInterestState, roiID);
    }

    @Override
    public List<regionOfInterestState> getRoiStatesOfImage(int image_id) {
        return imageDataDataSource.getRoiStatesOfImage(image_id);
    }

    @Override
    public List<RoiPoint> getPointsByRoiId(int roiId) {
        return imageDataDataSource.getPointsForRoi(roiId);
    }

    public List<String> getRoiTags(int roiID) {
        return imageDataDataSource.getRoiTags(roiID);
    }

    @Override
    public void addNewRoiTag(RoiTag roiTag) {
        imageDataDataSource.addNewRoiTag(roiTag);
    }

    @Override
    public void addNewRoi(regionOfInterestState regionOfInterestState) {
        System.out.println(regionOfInterestState.getId() + " " + regionOfInterestState.getCo2() + " " + regionOfInterestState.getO2() + " " + regionOfInterestState.getPh());
        int roi_id = imageDataDataSource.addNewRoi(regionOfInterestState.getId());
        addNewState(regionOfInterestState, roi_id);

    }

    @Override
    public void removeRoiTag(RoiTag roiTag) {
        imageDataDataSource.removeRoiTag(roiTag.getId(), roiTag.getTag());
    }

    /**
     * Returns list of all image attributes from images in a given directory path
     * @param path directory path containing images
     * @return list of all image attributes from images in a given directory path
     *
     * @author Kim Chau Duong
     */
    @Override
    public List<ImageAttribute> getAllDataFromPath(String path) {
        return imageDataDataSource.showDataFromPath(path);
    }

    /**
     * Returns list of image attributes that match a given file path
     * @param path path of file
     * @return list of image attributes that match a given file path
     *
     * @author Kim Chau Duong
     */
    @Override
    public List<ImageAttribute> getDataFromFilePath(String path) {
        return imageDataDataSource.showDataFromFilePath(path);
    }

    /**
     * Calls datasource class method to return a list of unique tags of ROIs of an image
     * @param imageId id of image that contains the ROI tags
     * @param page current unique tag page
     * @param size amount of tags shown per page
     * @return list of unique tags of ROIs of an image
     *
     * @author Kim Chau Duong
     */
    @Override
    public List<String> getUniqueImageTags(int imageId, int page, int size) {
        return imageDataDataSource.getUniqueImageTags(imageId, page, size);
    }

    /**
     * Calls datasource class method to return a list of unique tags of ROIs of images in a folder
     * @param path Folder path that contains images that contain ROI tags
     * @param page current unique tag page
     * @param size amount of tags shown per page
     * @return list of unique tags of all ROIs of all images in a folder
     *
     * @author Kim Chau Duong
     */
    @Override
    public List<String> getUniqueFolderTags(String path, int page, int size) {
        return imageDataDataSource.getUniqueFolderTags(path, page, size);
    }

}
