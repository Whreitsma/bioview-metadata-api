package nl.bioinf.bioview.metadataapi.data_access.jdbc;

import nl.bioinf.bioview.metadataapi.Model.ImageAttribute;
import nl.bioinf.bioview.metadataapi.Model.ImageMetadata;
import org.springframework.jdbc.core.RowMapper;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;


public class ImageDataRowMapper implements RowMapper<ImageAttribute> {

    @Override
    public ImageAttribute mapRow(ResultSet result, int rowNumber) throws SQLException {
        ImageAttribute imageAttribute = new ImageAttribute();
        imageAttribute.setImageName(result.getString("name"));
        imageAttribute.setPath(result.getString("path"));
        imageAttribute.setDateCreated(result.getString("date"));
        imageAttribute.setFileType(result.getString("type"));
        imageAttribute.setImageSize(result.getLong("size"));

        return imageAttribute;



    }
}
